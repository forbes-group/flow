Notes
=====

## 2 June 2024

I added a crude stochastic potential -- purely random gaussian variables -- and couple
this with complex-time cooling to fix the energy.  We should play a bit with this to see
what happens.  Maybe look at the spectra as a function of time.  We might want to try to
pump the system in a spectrally dependent way.  I added some options limit the cooling
size and an option to disallow "negative" cooling that increases the energy - only the
stochastic potential will allow energy increases.  Note: this should only be used with
Split operator since the potential is not smooth.  Added a filter.

Independent of above, I am playing with randomly locating the vortices to get rid of
cloud-splitting.  Specifically, when imprinting a regular grid of solitons, we have two
large momentum components that split the cloud into opposite directions.  These two
oscillating clouds go in and out of phase giving rise to the large oscillations in x_cm
seen before it dies down.

Another note: Reeves:2015 discusses the quantum to semi-classical transition at about
`D = 12*healing_length` for vortices.  This corresponds to: 
`V_sigma_healing_length = np.sqrt(D**2/ln(V0_mu)/8)`

## 1 June 2024

Saptarshi had the idea to move the potential instead of letting the cloud oscillate.
This has some advantages, for example, we can cool to a nice stationary state, then
start from zero motion.  However, we must be careful to match the trap frequency for
resonance.  For harmonic traps, this should not be a problem.

For now I am still playing with the the oscillating cloud (see `Oscillations.md`).  A
couple of notes:
1. The way I am generating solitons causes the cloud to split in two multiple times.
   This is very artificial and we should fix.  One possibility might be to just allow
   the system to evolve until some low moments (not dipole of course) calm down.
2. There are certainly revivals in this system due to the oscillations of the two bulges
   above.
3. Should still play with solitons spaced further apart.  (3 healing lengths is pretty
   close).
4. Probably should try to normalize so that states have the same energy *after* finger
   is turned on.  The energy should ultimately thermalize, and so should be correlated
   with the temperature.
5. The viscosity should have some interpretation as heating the system.  How do we see
   this given conserved energy?  Separation of bulk flow from thermalization?
6. Should fairly quickly determine a scale for the viscosity so we make sure we are
   seeing reasonable phenomena.  I.e., if I make $\sigma$ larger, do we see the expected
   slow-down?  Do the 1D calculation to get a scale.


