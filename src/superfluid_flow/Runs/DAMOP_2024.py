import os.path
from pathlib import Path
import sys

from superfluid_flow import flowHO1d
from gpe.utils import Simulation

import numpy as np


class ExperimentDAMOP_1a(flowHO1d.ExperimentSolitons):
    Nx = 2**10
    Ns = 50
    healing_length_dx = 2.0
    x0_R = 0.1
    x_TF_R = 0.3
    V0_mu = 0.1
    Ntot = 1.0
    initial_state = "random_solitons"


class ExperimentDAMOP_2a(ExperimentDAMOP_1a):
    """Stronger finger."""

    V0_mu = 0.2


class ExperimentDAMOP_2b(ExperimentDAMOP_2a):
    seed = 10


class ExperimentDAMOP_3a(ExperimentDAMOP_2a):
    """Larger finger"""

    V_sigma_healing_length = 15  # Should be in semi-classical regime


class ExperimentDAMOP_4a(flowHO1d.ExperimentSolitons):
    Nx = 2**10
    Ns = 0

    healing_length_dx = 2.0
    x0_R = 0.1
    x_TF_R = 0.3
    V0_mu = 0.2
    V_sigma_healing_length = 10
    Ntot = 1


class ExperimentDAMOP_4b(ExperimentDAMOP_4a):
    x0_R = 0.3


if __name__ == "__main__":
    _locals = locals()

    if len(sys.argv) == 2:
        Experiments = [_locals[sys.argv[1]]]
    else:
        Experiments = [_locals[E] for E in _locals if E.startswith("Experiment")]

    for Experiment in Experiments:
        e = Experiment()
        e.run(show=False)
