---
jupytext:
  formats: md:myst,ipynb
  notebook_metadata_filter: jupytext_format_version,language_info
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.2
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
language_info:
  codemirror_mode:
    name: ipython
    version: 3
  file_extension: .py
  mimetype: text/x-python
  name: python
  nbconvert_exporter: python
  pygments_lexer: ipython3
  version: 3.11.9
---

```{code-cell} ipython3
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
import mmf_setup;mmf_setup.nbinit()
import numpy as np
import matplotlib.pyplot as plt
from importlib import reload
%load_ext autoreload
%autoreload
from hydro import piston_1D_up
import cProfile
from IPython.display import clear_output
```

## 1D Piston-Shock Problem

Here we study how a BEC trapped in a harmonic potential interacts with a piston going from right to left. We have tranformed the governing GPE to 1D SWE by making use of axially symmetric 2D system. The governing 1D SWE is
\begin{gather}
\dot{n} = -\nabla \left(nu\right), \quad j=nu,\\
\dot{j} = -\nabla\left(nv^2\right) - \frac{n}{m}\nabla\left(V_\text{int} + V_\text{ext}\right) + \nu \nabla^2u,\\
\dot{j} = -\nabla\left(nv^2 + \frac{n V_\text{int}}{m}\right) - \frac{n'V_\text{int}}{m} - \frac{nV'}{m} + \nu \nabla^2u.
\end{gather}

+++

### External Potential

Here the external potential has contribution from the trap and the piston.
The atoms are confined in a harmonic trap:
$$
    V_{\text{trap}} = \frac{1}{2}m\omega^2 x^2,
$$
and the piston is modelled as a very sharp Gaussian, moving in from the right with a constant velocity:
$$
    V_{\text{t}} = V_{\text{piston}} = V_0 \exp\bigg({-\frac{(x_0 - v_p t)^2}{2\sigma_p^2}}\bigg).
$$

+++

### The Formulation (Finite Volume Method):

\begin{gather}
\dot{\rho} = -\frac{\partial F}{\partial x} + \mathcal{S} \\
\frac{d \rho}{dt} =  - \frac{1}{\bigtriangleup x} (F_{xR} - F_{xL}) + \mathcal{S}_{x} \\
F_{xR} = \frac{1}{2} \left((F_{x+dx} + F_{x}) - |\alpha_{R}|(\rho_{x+dx} - \rho_{x}) \right) \\
F_{xL} = \frac{1}{2} ((F_{x} + F_{x-dx}) - |\alpha_{L}|(\rho_{x} - \rho_{x-dx})) \\
\alpha_I = \frac{\bigtriangleup F}{\bigtriangleup \rho} \quad \bigtriangleup \rho > 1e-6 \\
 = \frac{\partial F}{\partial \rho} \quad otherwise
\end{gather}

\begin{gather}
\frac{\partial j}{\partial t} = -\frac{1}{2*\bigtriangleup x} \left(F_{x+dx} - F_{x-dx} - (\alpha_R j_{x+dx} - (\alpha_R + \alpha_L)j_{x} + \alpha_L j_{x-dx}) \right) + \mathcal{S_x}
\end{gather}

For our current equation, this becomes

```{code-cell} ipython3
solve?
```

```{code-cell} ipython3

```

## Sympy solution for polynomial in $\sigma^2$

```{code-cell} ipython3
import sympy as sp

x, p, q = sp.var("x, p, q")

f = x**6 + p*x + q

# sp.roots(f, x)
sp.solve(f, x)
```

```{code-cell} ipython3
roots?
```

```{code-cell} ipython3

```

## FVM with SWE

```{code-cell} ipython3
%autoreload
from gpe.bec import u
from hydro.piston_1D_up import ExperimentBEC, ExperimentGaussian

#expt = ExperimentBEC(dt_=1e-3, x_TF=4*u.micron, g=None, nu=200*u.micron*u.Hz, 
#u_piston = 2*u.mm*u.Hz, V0p=3.4*10, p_sigma=0.2*u.micron, Lx=10*u.micron, x0=5*u.micron)

expt = ExperimentBEC(x_TF=200*u.micron, g=None, nu_=20, 
    u_piston = 2*u.mm*u.Hz, V0p=3.4*1e5, p_sigma=11*u.micron, Lx=464*u.micron, x0=280*u.micron)

s = expt.get_state()
s.plot()
```

```{code-cell} ipython3
#cProfile.run("s.evolve_FE(steps=1, t__final=1, show_plot=False)", sort=1);
s.evolve(hist=False, steps=1e3, dt_t_scale=100);
#h2 = s.evolve(hist=True, steps=1e3, t__final=4e5, dt_t_scale=100);
#h1 = s.evolve(hist=True, steps=1e5, t__final=70, dt_t_scale=0.1, JT=True); #nu=25
#h1[-1].plot()
```

## Generate test data

```{code-cell} ipython3
%autoreload
from gpe.bec import u
from hydro.piston_1D_up import ExperimentBEC

expt = ExperimentBEC(x_TF=4*u.micron, g=None, nu_=200, 
                     u_piston=2*u.mm*u.Hz, V0p=3.4*10,
                     p_sigma=0.2*u.micron, Lx=10*u.micron,
                     x0=5*u.micron)

s = expt.get_state()

h1 = s.evolve(hist=False, show_plot=False, steps=1e2, t__final=2*u.ms, 
              dt_t_scale=0.1, JT=False, fname="");
```

```{code-cell} ipython3
%autoreload
from gpe.bec import u
from hydro.piston_1D_up import ExperimentGaussian

expt = ExperimentGaussian(Lx=4*u.micron, dx=0.1*u.micron, sigma_g=0.4*u.micron,
                          A_g=4.3e2/u.micron, nu_=25*4e3)

s = expt.get_state()

s = s.evolve(hist=False, show_plot=False, steps=1e3, t__final=4*u.ms, 
             dt_t_scale=100, fname="test_density");
```

```{code-cell} ipython3
s_ = expt.get_state()
s_.plot()
```

```{code-cell} ipython3
s.plot()
```

```{code-cell} ipython3
%autoreload
from gpe.bec import u
from hydro.piston_1D_up import ExperimentBEC, ExperimentGaussian

expt = ExperimentBEC(dt_=1e-3, x_TF=4*u.micron, g=None, nu=500*u.micron*u.Hz, 
u_piston = 2*u.mm*u.Hz, V0p=3.4*10, p_sigma=0.2*u.micron, Lx=10*u.micron, x0=5*u.micron)

s = expt.get_state()
s.plot()
```

```{code-cell} ipython3
h2 = s.evolve(hist=True, steps=100, t__final=70, dt_t_scale=0.1, JT=True); #nu=50
```

```{code-cell} ipython3

```

```{code-cell} ipython3
x = h1.x
y1, y2 = h1.get_density(), h2.get_density()
plt.plot(x, (y2-y1)/y1)
```

```{code-cell} ipython3
plt.plot(x, y1, x, y2)
plt.xlim(0, 2)
```

```{code-cell} ipython3
%autoreload
from gpe.bec import u
from hydro.piston_1D_up import ExperimentBEC, ExperimentGaussian

expt1 = ExperimentBEC(dt_=1e-3, x_TF=4*u.micron, g=None, nu=25*u.micron*u.Hz, 
u_piston = 2*u.mm*u.Hz, V0p=3.4*10, p_sigma=0.2*u.micron, Lx=10*u.micron, x0=5*u.micron)

s1 = expt.get_state()
s1.plot()
```

```{code-cell} ipython3
h3 = s1.evolve_FE(hist=True, steps=1e3, t__final=120, dt_t_scale=0.1);
```

```{code-cell} ipython3
%autoreload
from gpe.bec import u
from hydro.piston_1D_up import ExperimentBEC, ExperimentGaussian

expt2 = ExperimentBEC(dt_=1e-3, x_TF=4*u.micron, g=None, nu=2500*u.micron*u.Hz, 
u_piston = 2*u.mm*u.Hz, V0p=3.4*10, p_sigma=0.2*u.micron, Lx=10*u.micron, x0=5*u.micron)

s2 = expt2.get_state()
s2.plot()
```

```{code-cell} ipython3
h4 = s2.evolve_FE(hist=False, steps=2e3, t__final=70, dt_t_scale=0.1);
```

```{code-cell} ipython3
expt = piston.Experiment(basis_type='axial', Lx=10*u.micron, R=0.1*u.micron,
                       trapping_frequencies_Hz=(2.4*1600, 222.0*1600, 222.0*1600),
                       tube=True, x_TF=4*u.micron, v_p_mm_s=2.0,# V_p=0.03,
                       x_p0_x_s=None, x_p0=4.0, x_p=0.3*u.micron, r_p=1.7*u.micron)
```

```{code-cell} ipython3
import numpy as np
l = np.diag(np.array([0.5, 0.3]))
R = np.array([[1, 0], [0, 1]])
A = np.dot(np.dot(R, l), np.linalg.inv(R))
R1, l1 = np.linalg.eig(A)
b = np.array([0,1]).reshape(2,1)
b1 = np.array([0,1])
C = np.dot(A, b)
C1 = np.dot(A, b1)
d = np.zeros((2,1))
print(0.071**2/(2*250*u.Hz), 0.017/(100+np.sqrt((1)*1500/s.m)), s.dt)

print(np.shape(C1+d), np.shape(C+d))
```

```{code-cell} ipython3
%autoreload
from gpe.bec import u
from hydro.piston_1D_up import ExperimentDamBreak, ExperimentGaussian
"""
expt = ExperimentDamBreak()
"""
expt = ExperimentDamBreak()
s = expt.get_state()
s.plot()
```

```{code-cell} ipython3
s.evolve_FE(hist=False, steps=100, t__final=4e7, dt_t_scale=0.1);
```

```{code-cell} ipython3
print(s.nu, s.g)
```

# Central barrier test

```{code-cell} ipython3
%autoreload
from gpe.bec import u
from hydro.piston_1D import ExperimentPokey

expt = ExperimentPokey(dt_=1e-6, x_TF=200, g=0.0007682083478971493/8.61, V0p=3.4e3, nu=25*u.micron*u.Hz*1e2)
s = expt.get_state()
s.plot()
```

```{code-cell} ipython3
s.evolve_FE(hist=False, steps=1e3, t__final=4e7, dt_t_scale=1e-2);
```

```{code-cell} ipython3
#np.save('../tests/Test_FVM_Base.npy', s.get_density())
#y = np.load('Test_FVM_Base.npy')
#plt.plot(y)
```

```{code-cell} ipython3
species = s.experiment.species
a = u.scattering_lengths[(species, species)]
a_perp = np.sqrt(u.hbar/s.m/s.omega[-1])
n = s.get_density()
sigma2 = a_perp**2 * np.sqrt(1 + 2*a*n)
g_ = s.g / (2 *np.pi * sigma2)
print(s.g/g_.max())
```

# UFG

+++

## 1D John-Thomas Expt.

Here we study a UFG trapped in a harmonic potential. The governing 1D SWE is
\begin{gather}
\dot{n} = -\nabla \left(nu\right), \quad j=nu,\\
\alpha = \xi \frac{\hbar^2(3\pi^2)^{2/3}}{m}, \qquad m = m_D = 2 m_F,\\
\dot{j} = -\nabla\left(nu^2 + \frac{3}{5}\alpha n^{5/3}\frac{2^{5/3}}{m}\right) - \frac{nV'}{m} + \nu \nabla^2u.
\end{gather}

```{code-cell} ipython3
%autoreload
from gpe.bec import u
from hydro.piston_1D import ExperimentJohnThomas

expt = ExperimentJohnThomas()
s = expt.get_state()
s.plot()
```

```{code-cell} ipython3
s.evolve_FE(dt_t_scale=1e-2)
```

## Misc Checks

```{code-cell} ipython3
%autoreload
from hydro import piston_1D
s = piston_1D.StateNew(V0p=0, x0=30, method='LLF', nu=0, omega=(2.4, 222, 222))
s.plot()
```

```{code-cell} ipython3
plt.plot(s.x, s.get_Vt(), sf.x, sf.get_Vt())
```

```{code-cell} ipython3
import numpy as np

a = [1, 2, 3, -2, -4, 4, 5, 6]
c = [1, 2, 2, 2, 3, 4, 1, 1]
d = [1, 1, 1, 1, 1, 1, 1, 1]
#np.maximum(c, np.minimum(3, np.where(np.array(a)>3, 4, a)))
c = np.minimum(np.minimum(a, c), 1)
print(c)
```

## Sort

```{code-cell} ipython3
import matplotlib.pyplot as plt
tms = 10
tms_code = tms*63.50779925891489
s = piston_1D.State1(issrc=True, nu=0.5, dx=0.5)
n0, j0 = s.data
print(max(n0))
t_final = tms_code/s.time_scale
N0 = s.get_N()
print(s.dt/s.time_scale, t_final)
#hist = s.evolve_to(t_final, return_history=True, rtol=1e-12, atol=1e-12)
sf = s.evolve_to(t_final, rtol=1e-12, atol=1e-12)
```

```{code-cell} ipython3
print(np.shape(hist[0,:,1]), s.u_piston, s.nu)
for idx in [0, 400, 1000, 2000, 4000, 6000]:
    plt.plot(s.x, hist[0,:,idx])
    #plt.plot(hist[idx].x, hist[idx].data[0,s.cs])
#plt.plot(hist[0].x, hist[0].data[0,s.cs])
```

```{code-cell} ipython3
print(np.shape(hist), s.u_piston)
for idx in [0, 400, 1000, 2000, 4000]:
    plt.plot(hist[idx].x, hist[idx].data[0,s.cs])
#plt.plot(hist[0].x, hist[0].data[0,s.cs])
```

```{code-cell} ipython3
s = piston_1D.State(Lx = 8e-5, x0=264.184*1e-7, issrc=True)
n0 = np.zeros((s.N,))
factor = 1

n0 = factor*np.exp(-s._x**2/2.0)
j0 = np.zeros_like(n0)
n0, j0 = s.extend_nj_with_bc((n0[s.cs], j0[s.cs]))
s.data = np.array((n0, j0))

t_final = 0.01*s.second
N0 = s.get_N()
print(s.dt)
hist = s.evolve_to(t_final, return_history=False, rtol=1e-12, atol=1e-12)
```

```{code-cell} ipython3
print(min(s.ts)/s.second, max(s.ts)/s.second) 
```

```{code-cell} ipython3
from IPython.display import clear_output, display
import matplotlib.pyplot as plt
import time
import numpy as np
import warnings
warnings.filterwarnings("ignore")
print(np.shape(hist))

#plt.ion()
#fig, ax = plt.subplots(1, 2)
#ax1 = plt.subplot(221)
#ax2 = plt.subplot(222)
for tidx in range(12):
    if tidx%1==0:
        plt.clf()
        plt.subplot(221)
        plt.plot(s.x, hist[0,:,tidx])
        plt.subplot(222)
        plt.plot(s.x, hist[1,:,tidx])
        plt.subplot(223)
        plt.plot(s.x, s.get_Vt(s.x, s.ts[tidx]))
        time.sleep(.8)
        display(plt.gcf())
        clear_output(wait=True)
        #plt.close('all')

# + cocalc={"outputs": {"1": {"name": "input", "opts": {"password": false, "prompt": "ipdb> "}, "output_type": "stream", "value": "n"}}}
x = np.linspace(-40, 40, 100)
plt.plot(x, s.get_Vt(x, 2))
```

# Optimization

+++

Comments:

1. `gpe.utils.ExperimentBase.__getattr__` is a significant slowdown, and only for debugging purposes.  We need to add a toggle that disables this when doing actual runs.  (I have currently disabled it completely.)
2.

```{code-cell} ipython3
from IPython.display import clear_output
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
import mmf_setup;mmf_setup.nbinit()
clear_output()
import numpy as np
import matplotlib.pyplot as plt
from importlib import reload
from gpe import utils
from hydro import piston_1D;reload(piston_1D)
import cProfile, pstats
from gpe.bec import u

%load_ext autoreload
```

```{code-cell} ipython3
%autoreload
from hydro.piston_1D import ExperimentBEC, ExperimentGaussian, StateFV

expt = ExperimentGaussian(Lx=4*u.micron, dx=0.1*u.micron, sigma_g=0.4*u.micron, A_g=4.3e2/u.micron, nu=25*u.micron*u.Hz*4e3)
s = expt.get_state()
```

```{code-cell} ipython3
prof = cProfile.Profile()
cProfile.run('s.evolve_FE(hist=True, steps=1000, t__final=40000, dt_t_scale=100, show_plot=False)', 'prof.data')
p = pstats.Stats('prof.data')
```

```{code-cell} ipython3
#p.strip_dirs().sort_stats(pstats.SortKey.CUMULATIVE).print_stats()
p.strip_dirs().sort_stats(pstats.SortKey.TIME).print_stats()
```

```{code-cell} ipython3
%load_ext line_profiler
# %lprun -f piston_1D.FVMixin.get_dU_dt s.evolve_FE(hist=True, steps=1000, t__final=40000, dt_t_scale=100, show_plot=False)
```

```{code-cell} ipython3
N = 30000
h = 1.1 * np.arange(N)
hu = 1.1 * np.arange(N)
h[1] = 0.0

_TINY = np.finfo(float).tiny

def f1():
    res = hu / h
    return res

def f2():
    return (h > 0.1) * hu/h

def f3():
    return np.where(h > 0.1, hu/h, 0)

def f4():
    with np.errstate(all='ignore'):
        res = np.where(h > 0.1, hu/h, 0)
    return res

def f5():
    res = (1 + np.sign(h-0.1))/2 * hu / (h + _TINY)
    return res

def f6():
    _cond = (h > 0.1)
    res =  _cond * hu / (h + _TINY)
    return res

def f6():
    global h
    h += _TINY
    _cond = (h > 0.1)
    res = hu
    res /= h
    res *= _cond
    return res

with np.errstate(all='ignore'):
    # %timeit res = f1()
    # %timeit res = f2()
    # %timeit res = f3()
#%timeit res = f5()
#%timeit res = f2()
#%timeit res = f3()
#%timeit res = f4()
```

%lprun -f utils.ExperimentBase.__getattribute__ s.evolve_FE(hist=True, steps=1000, t__final=40000, dt_t_scale=100, show_plot=False)

```{code-cell} ipython3

```

# Mock up viscosity dependence on time

```{code-cell} ipython3
ys_ = nu0, a, b, c = 0, 2, 7, 5
ts = (0, 60, 100, 140)

ms = (2/60, 5/40, -2/40)

# c = y - m*x
cs = (0, 2 - 5*3/2, 7 + 5)

def get_x_y(i):
    ts_ = np.linspace(ts[i], ts[i+1], 10)
    nus_ = ms[i] * ts_ + cs[i]
    return (ts_, nus_)
```

```{code-cell} ipython3
fig = plt.figure()
gs = fig.add_gridspec(1, 1)

ax = fig.add_subplot(gs[0])
xs = ts
ys = ys_
ax.plot(xs, ys)

# First segment
i = 0
x, y = get_x_y(i)
ax.plot(x, y, marker="o")

# Secondsegment
i = 1
x, y = get_x_y(i)
ax.plot(x, y, marker="o")

# Third segment
i = 2
x, y = get_x_y(i)
ax.plot(x, y, marker="o")
```

```{code-cell} ipython3
sum(75 > np.asarray(list(ts))) - 1
```

## Test time-dependent viscosity

```{code-cell} ipython3
%autoreload
from hydro.piston_1D_up import ExperimentViscosity3Pt

expt = ExperimentViscosity3Pt()
state = expt.get_state()
state.plot()
```

```{code-cell} ipython3
state.evolve(steps=2000);
```

```{code-cell} ipython3

```
