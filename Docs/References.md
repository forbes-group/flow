(sec:readings)=
Resources, Readings, and References
===================================


* {cite}`Reeves:2015`: GPE. Considers vortex shedding from a gaussian barrier in 2D flow to
  define a Reynolds number related to the Strouhal.  Uses a result of Ramanujan to
  remove vortex-antivortex pairs so that the simulations can be run for a long time.

This paper is [cited by the following](https://www.webofscience.com/wos/woscc/summary/438d33bf-c670-49d8-81ba-9163b12fb831-ec02ac5d/date-descending/1).
  
* {cite}`Doran:2024`: PGPE. Considers flow through a point-like disordered potential.
  Looks at slowing to get an effective viscosity.
* {cite}`Kokubo:2024`: Moves a gaussian barrier through a 2D harmonic trap to determine the
  critical velocity - again looking at vortex shedding.
* {cite}`Kokubo:2024a`: GPE.  2D vortex shedding past a plate.  (Not very thorough.)
* {cite}`Liu:2024`: GPE.  2D vortex depinning in a flowing background.
* {cite}`Tang:2024`: GPE.  Counterflow... did not read too carefully, but might have
  some interesting things.  Tsuboto.

(sec:references)=
References
==========

```{bibliography}
:style: alpha
```
