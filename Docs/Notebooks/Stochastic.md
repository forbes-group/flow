---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.2
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Stochastic State Preparaion

```{code-cell} ipython3
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
rng = np.random.default_rng(seed=2)
L = 1.0
N = 256
dx = L/N
Ns = 256
k = 2*np.pi * np.fft.fftfreq(N, dx)
nk = (abs(np.fft.fft(rng.normal(size=(N, Ns), scale=1/np.sqrt(N)), axis=0)**2)).mean(axis=1)
plt.plot(k, nk, '+')
plt.ylim(0, 2)
```

```{code-cell} ipython3
%matplotlib inline
%load_ext autoreload
%autoreload 2
import numpy as np, matplotlib.pyplot as plt
from IPython.display import clear_output
import mmfutils.performance.fft
mmfutils.performance.fft.set_num_threads(1)

from pytimeode.evolvers import EvolverSplit

import superfluid_flow
from superfluid_flow import flowHO1d
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import FPS


class Experiment(flowHO1d.StochasticMixin, flowHO1d.ExperimentBase):
    x_TF_R = 0.4
    x0_R = 0.0
    def get_Vext(self, state, gpu=False):
        Vext = super().get_Vext(state, gpu=gpu)
        Vext += self.get_stochastic_V(state, gpu=gpu)
        return Vext

    def get_moments(self, state, N=4):
        x, = state.xyz
        p = np.arange(N)
        n = state.get_density()
        return np.sum(n*x[None, :]**p[:, None], axis=-1)/np.sum(n)
    
e = Experiment(x0_R=0, V0_mu=0.1,
               E=None,
               stochastic_cool=0.1,
               stochastic_kc_kmax=0.5,
               stochastic_cool_only=True,
               stochastic_V0_mu=0.5, 
               initial_state='regular_solitons')
s = e.get_initial_state()
moments = [e.get_moments(s)]
s.cooling_phase = 1+0.001j
ev = EvolverSplit(s, dt=0.4*s.t_scale, normalize=True)
steps = 100
states = [ev.get_y()]

ts = [s.t]
moments = [e.get_moments(s)]

fig, axs = plt.subplots(2, 1, height_ratios=[1, 5], gridspec_kw=dict(hspace=0.4))

for frame in FPS(frames=1000):
    ev.evolve(steps)
    ax = axs[1]
    ax.cla()
    plt.sca(axs[1])
    ev.y.plot()
    states.append(ev.get_y())
    ts.append(ev.y.t)
    moments.append(e.get_moments(ev.y))
    ax = axs[0]
    ax.cla()
    ax.plot(ts, np.divide(moments, np.max(np.abs(moments), axis=0)))
    clear_output(wait=True)
    display(fig)
plt.close('all')
```

```{code-cell} ipython3
ns = np.array([s.get_density() for s in states])
ts = np.array([s.t for s in states])
xs = states[-1].xyz[0]
plt.pcolormesh(ts, xs, ns.T)
```

```{code-cell} ipython3
import numpy as np
rng = np.random.default_rng(seed=2)
A = rng.random(size=2**12*3)
import mmfutils.performance
mmfutils.performance.fft.set_num_threads(4)
import mmfutils.performance.fft as fft
%timeit a = fft.ifft(fft.fft(A))
%timeit a = np.sin(A).sum()
```

```{code-cell} ipython3
%matplotlib inline
%load_ext autoreload
%autoreload 2
import numpy as np, matplotlib.pyplot as plt
from IPython.display import clear_output
import mmfutils.performance.fft
mmfutils.performance.fft.set_num_threads(1)

import superfluid_flow
from superfluid_flow import flowHO1d
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import FPS

u = flowHO1d.u

class Experiment(flowHO1d.Experiment):
    """Realistic experiment."""
    State = flowHO1d.StateGPU
    Lxyz = (688.4235*u.micron,)
    Nxyz = (2**12*3,)
    healing_length_dx = 2.0
    x_TF_R = 0.5361
    n0 = 3387.0805 / u.micron
    V_sigma_healing_length = 50.0
    x0_R = 0.1
    hbar = u.hbar
    m = u.m_Rb87
    
e = Experiment(t_cool=10.0, phi0=2*np.pi)
s = e.get_initial_state(cool=True)
s.plot()

res = e.evolve(s, steps=1000, skip=1, timeout=60*60*60)
```

```{code-cell} ipython3
# Resume after a disconnect
res = e.evolve(states=e._states, steps=1000, skip=1, timeout=60*60*60)
```

## Smaller Traps

```{code-cell} ipython3
%matplotlib inline
%load_ext autoreload
%autoreload 2
import numpy as np, matplotlib.pyplot as plt
from IPython.display import clear_output
import mmfutils.performance.fft
mmfutils.performance.fft.set_num_threads(1)

import superfluid_flow
from superfluid_flow import flowHO1d
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import FPS

u = flowHO1d.u

f = 16
class Experiment(flowHO1d.Experiment):
    """Realistic experiment."""
    State = flowHO1d.StateGPU
    Lxyz = (688.4235*u.micron/f,)
    Nxyz = (2**12*3//f,)
    healing_length_dx = 2.0
    x_TF_R = 0.3
    n0 = 3387.0805 / u.micron
    V_sigma_healing_length = 10.0
    x0_R = 0.1
    hbar = u.hbar
    m = u.m_Rb87
    V0_mu = 0.5
    
e = Experiment(t_cool=1.0, initial_state="regular")
print(e.t_unit/u.ms)
s = e.get_initial_state(cool=True)
s.plot()
res = e.evolve(s, steps=1000, skip=1, timeout=60*60*60)
```

```{code-cell} ipython3
s.t = 1.0
s.initializing = True
plt.plot(s.get_xyz()[0]/e.healing_length, e.get_Vext(s))
```

```{code-cell} ipython3
%load_ext autoreload
%autoreload 2
import superfluid_flow
from superfluid_flow import flowHO1d

e = flowHO1d.Experiment(t_cool=0.01, phi0=0.1)
s = e.get_initial_state(cool=True)
s.plot()
```

```{code-cell} ipython3
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
from IPython.display import clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import FPS

e = flowHO1d.Experiment(t_cool=0.02, phi0=2*np.pi)
s = e.get_initial_state(cool=True)
s.plot()
```

```{code-cell} ipython3
e = flowHO1d.Experiment(t_cool=0.02, phi0=2*np.pi, Lxyz=(100.0,), Nxyz=(256*4,), 
                        x_TF_R=0.2, V0_mu=1)
s = e.get_initial_state(cool=True)
res = e.evolve(s, skip=100, timeout=60*60)
```

```{code-cell} ipython3
e = flowHO1d.Experiment(t_cool=0.02, phi0=2*np.pi, Lxyz=(100.0,), Nxyz=(256*4,), 
                        x_TF_R=0.2, V0_mu=0.1)
s = e.get_initial_state(cool=True)
res = e.evolve(s, skip=100, timeout=60*60)
```

```{code-cell} ipython3
#s.cooling_phase = 1+0.1j
ev = EvolverABM(s, dt=0.1*s.t_scale)
steps = 200
fig, axs = plt.subplots(1, 2, figsize=(10, 5))
x_cms = [s.get_x_cm()]
ts = [s.t]
skip = 100
for frame in FPS(frames=10000, timeout=60*5):
    ev.evolve(steps)
    ts.append(ev.y.t)
    x_cms.append(ev.y.get_x_cm())
    if frame % skip == 0:
        ax = axs[0]
        ax.cla()
        plt.sca(ax)
        ev.y.plot()

        ax = axs[1]
        ax.cla()
        plt.sca(ax)
        plt.plot(ts, x_cms)
        #plt.axhline(np.mean(Fs))
        #ax.set(ylim=(-1,1))
        display(fig)
        clear_output(wait=True)
```

```{code-cell} ipython3
e = flow1d.Experiment(t_cool=0.01, phi0=0.0, V_v_c=0.5, V0_mu=0.5, V_t_=2.0)
s = e.get_initial_state()
s.plot()
```

```{code-cell} ipython3
s.cooling_phase = 1+1j
ts, ps, Fs =  e.evolve(s, show=True)
```

```{code-cell} ipython3
from scipy.signal import savgol_filter

plt.plot(ts, savgol_filter(ps, window_length=100, polyorder=1))
plt.plot(ts, ps, alpha=0.5)
plt.plot(ts, ps)
```

```{code-cell} ipython3
from scipy.signal import savgol_filter

plt.plot(ts, savgol_filter(ps, window_length=100, polyorder=1))
plt.plot(ts, ps, alpha=0.5)
plt.plot(ts, ps)
```

```{code-cell} ipython3
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
from IPython.display import clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import FPS

e = flow1d.Experiment(t_cool=0.0, phi0=0.5, V_v_c=0.8)
s = e.get_initial_state(cool=True)
s.plot()
```

```{code-cell} ipython3
#s.cooling_phase = 1+0.1j
ev = EvolverABM(s, dt=0.1*s.t_scale)
steps = 200
fig, axs = plt.subplots(1, 2, figsize=(10, 5))
Fs = [s.get_force()]
ps = [s.get_momentum()]
ts = [s.t]
skip = 10
for frame in FPS(frames=10000, timeout=60*5):
    ev.evolve(steps)
    ts.append(ev.y.t)
    Fs.append(ev.y.get_force())
    ps.append(ev.y.get_momentum())
    if frame % skip == 0:
        ax = axs[0]
        ax.cla()
        plt.sca(ax)
        ev.y.plot()

        ax = axs[1]
        ax.cla()
        plt.sca(ax)
        Fmax = np.abs(Fs).max()
        pmax = np.abs(ps).max()
        plt.plot(ts, np.divide(Fs, Fmax))
        plt.plot(ts, np.divide(ps, pmax))
        ax.set(title=f"{Fmax=:0.2g} ({np.mean(Fs):.4g}), {pmax=:0.2g} ({np.mean(ps):.4g})")
        #plt.axhline(np.mean(Fs))
        ax.set(ylim=(-1,1))
        display(fig)
        clear_output(wait=True)
```

```{code-cell} ipython3
from scipy.signal import savgol_filter

plt.plot(ts, savgol_filter(ps, window_length=100, polyorder=1))
plt.plot(ts, ps, alpha=0.5)
```

```{code-cell} ipython3
from scipy.signal import savgol_filter

plt.plot(ts, savgol_filter(ps, window_length=100, polyorder=1))
plt.plot(ts, ps, alpha=0.5)
```

```{code-cell} ipython3
# %#matplotlib inline
import numpy as np, matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

import mmfutils.performance.fft, mmfutils.plot
from pytimeode.evolvers import EvolverSplit

from gpe.bec import StateTwist_x, Units
from gpe import utils

mmfutils.performance.fft.set_num_threads(1)


class State(StateTwist_x):
    def __init__(self, experiment, **kw):
        self.experiment = experiment
        super().__init__(**kw)

    def get_xyz(self, gpu=False):
        if gpu:
            return self._xyz_
        else:
            return self.xyz

    def _get_Vext_(self):
        """Return the external potential."""
        return self.experiment.get_Vext(state=self, gpu=True)

    def get_force(self):
        dV = self.experiment.get_Vext(state=self, d=1)
        n = self.get_density()
        return self.integrate(dV * n)

    def get_momentum(self):
        psi = self.get_psi()
        return self.hbar * self.braket(self.basis.get_gradient(psi)[0]).imag

    def plot(self, log=False, label=None, comoving=False):  # pragma: nocover
        n = self.get_density()

        def scale(n):
            if log:
                return np.log10(n)
            return n

        n = scale(n)
        if comoving:
            x = self.xyz[0]
            xlabel = r"$x$ [$\xi$]"
        else:
            x = self.x_lab
            xlabel = r"$x_{\rm lab}$ [$\xi$]"
            inds = np.argsort(x)
            x, n = x[inds], n[inds]

        x_ = x / self.experiment.healing_length
        plt.plot(x_, n, label=label)
        ax = plt.gca()
        ax.set(xlabel=xlabel)
        E = self.get_energy()
        N = self.get_N()
        t = np.ravel(self.t)[0]
        t_ = t / self.experiment.t_unit
        t_name = self.experiment.t_name
        plt.suptitle(f"$t={t_:.4f}{t_name}$, ${N=:.4f}$, ${E=:.4f}$")


class Experiment:
    Nxyz = (256,)
    Lxyz = (10.0,)
    healing_length_dx = 2.0
    hbar = m = 1
    n0 = 1

    V0_mu = 0.1
    V_sigma_healing_length = 3.0
    V_t_ = 0.0  # How long to take to turn barrier on in units of t_unit
    V_v_c = 0.0  # Speed of potential in units of c_cound
    V_integrable_mu = 0.0  # How much to break integrability

    State = State
    phi0 = 0.01  # Size of random phases

    v_x_V_v = 1.0  # Speed of frame in units of V_v
    t_cool = 1.0

    def __init__(self, **kw):
        for key in kw:
            if not hasattr(self, key):
                raise ValueError(f"Unknown {key=}")
            setattr(self, key, kw[key])
        self.init()

    def init(self):
        dx = self.Lxyz[0] / self.Nxyz[0]
        self.healing_length = self.healing_length_dx * dx
        self.mu = self.hbar**2 / 2 / self.m / self.healing_length**2

        self.t_unit = self.hbar / self.mu
        self.t_name = r"\hbar/\mu"

        self.g = self.mu / self.n0
        self.V_sigma = self.V_sigma_healing_length * self.healing_length

        self.V0 = self.V0_mu * self.mu
        self.V0_t_ = utils.get_smooth_transition(
            [0, self.V0], durations=[0], transitions=[self.V_t_]
        )

        self.c_sound = np.sqrt(self.mu / self.m)
        self.V_v = self.V_v_c * self.c_sound
        self.v_x = self.v_x_V_v * self.V_v
        self.state_args = dict(
            experiment=self,
            Nxyz=self.Nxyz,
            Lxyz=self.Lxyz,
            g=self.g,
            hbar=self.hbar,
            m=self.m,
            v_x=self.v_x,
        )
        self.rng = np.random.default_rng(seed=2)

    def get_Vext(self, state, gpu=False, d=0):
        """Return the external potential or it's derivative."""
        t = state.t
        t_ = t / self.t_unit

        x_lab = state.get_xyz(gpu=gpu)[0] + state.v_x * t
        x0 = self.V_v * t

        Lx = self.Lxyz[0]
        x0 = (x0 - x_lab.min()) % Lx + x_lab.min()
        ks = np.arange(1, 5) * np.pi / Lx

        # Background to break integrability
        V = (
            self.V_integrable_mu
            * self.mu
            * sum(np.sin(k * x_lab) for a, k in zip(ks, [1.0, -2.0, 3.0, 1.0]))
        )
        V += self.V0_t_(t_) * sum(
            np.exp(-(((x_lab - x0) / self.V_sigma) ** 2) / 2)
            for x0 in [x0 - Lx, x0, x0 + Lx]
        )
        if d == 0:
            return V
        elif d == 1:
            dV = self.V0 * sum(
                -(x_lab - x0)
                / self.V_sigma**2
                * np.exp(-(((x_lab - x0) / self.V_sigma) ** 2) / 2)
                for x0 in [x0 - Lx, x0, x0 + Lx]
            )
            return dV
        else:
            raise NotImplementedError(f"{d=} not supported.")

    def get_state(self, cool=False):
        state = self.State(**self.state_args)
        V = self.get_Vext(state)
        n = np.maximum((self.mu - V) / self.g, 0)
        phi = 2 * self.phi0 * (self.rng.random(size=n.shape) - 0.5)
        state.set_psi(np.sqrt(n) * np.exp(1j * phi))
        if cool:
            state.cooling_phase = 1j
            dt = 0.1 * state.t_scale
            steps = max(int(np.ceil(self.t_cool / dt)), 2)
            ev = EvolverSplit(state, dt=self.t_cool / steps, normalize=True)
            ev.evolve(steps)
            psi = ev.y.get_psi()

            state = self.State(**self.state_args)
            state.set_psi(psi)
        return state


e = Experiment()
s = e.get_state()
s.plot()
```

```{code-cell} ipython3
from IPython.display import clear_output, display
from mmfutils.contexts import FPS
from pytimeode.evolvers import EvolverABM

e = Experiment(V_v_c=0.0, phi0=0.2, t_cool=1)
s0 = e.get_state(cool=False)
s0.plot()
plt.figure()
s1 = e.get_state(cool=True)
s1.plot()
```

```{code-cell} ipython3
assert np.allclose(s0.get_N(), s1.get_N())
s0.get_energy(), s1.get_energy()
```

```{code-cell} ipython3
f = 2
e = Experiment(V_v_c=0.2, V_sigma_healing_length=10, V_integrable_mu=0.01,
               V0_mu=0.1, phi0=0.1*np.pi, t_cool=0.1, Nxyz=(256*f,), Lxyz=(10.*f,))
s = e.get_state(cool=True)
s.cooling_phase = 1
ev = EvolverABM(s, dt=0.1*s.t_scale)
steps = 200
fig, axs = plt.subplots(1, 2, figsize=(10, 5))
Fs = [s.get_force()]
ps = [s.get_momentum()]
ts = [s.t]
skip = 10
for frame in FPS(frames=10000, timeout=60*5):
    ev.evolve(steps)
    ts.append(ev.y.t)
    Fs.append(ev.y.get_force())
    ps.append(ev.y.get_momentum())
    if frame % skip == 0:
        ax = axs[0]
        ax.cla()
        plt.sca(ax)
        ev.y.plot()

        ax = axs[1]
        ax.cla()
        plt.sca(ax)
        Fmax = np.abs(Fs).max()
        pmax = np.abs(ps).max()
        plt.plot(ts, np.divide(Fs, Fmax))
        plt.plot(ts, np.divide(ps, pmax))
        ax.set(title=f"{Fmax=:0.2g} ({np.mean(Fs):.4g}), {pmax=:0.2g} ({np.mean(ps):.4g})")
        #plt.axhline(np.mean(Fs))
        ax.set(ylim=(-1,1))
        display(fig)
        clear_output(wait=True)
```

```{code-cell} ipython3
s = ev.get_y()
plt.plot(s.x, np.gradient(s.get_Vext(), s.x))
plt.plot(s.x, s.experiment.get_Vext(s, d=1))
```

# Things to Check (for Chance)

+++

1. No matter what the state, if $V(x)$ is independent of time, then the total energy is conserved.  This is consistent with the fact that, even if there is a force, there is no displacement, so there is no work done.  Derive this.
2. A time-independent $V(x)$ does not imply that total momentum is conserved.
3. A moving potential $V(x-vt)$ can do both work (energy changes) and can change the momentum.
4. Thus, a time-independent potential $V(x)$ in a constantly moving frame must change both energy and momentum.  Explain why the derivation in 1) fails.

```{code-cell} ipython3
f = 2
kw = dict(V_v_c=0.2, V0_mu=0.1, Nxyz=(256*f,), Lxyz=(10.*f,), phi0=0.01)
e0 = Experiment(v_x_V_v=0, **kw)
e1 = Experiment(v_x_V_v=1, **kw)
s0 = e0.get_state(cool=False)
s1 = e1.get_state(cool=False)
s0.get_energy(), s1.get_energy()

ev0 = EvolverABM(s0, dt=0.1 * s0.t_scale)
ev1 = EvolverABM(s1, dt=0.1 * s0.t_scale)
[ev.evolve(10000) for ev in [ev0, ev1]]
print([ev.y.get_energy() for ev in [ev0, ev1]])
[ev.y.plot(comoving=False) for ev in [ev0, ev1]]
```

# Review Paper

+++

Here we explore some of the features described in the paper https://arxiv.org/abs/2306.05048: *"Stationary transport above the critical velocity in a one-dimensional superflow past an obstacle."*  Instead of solving for the stationary solutions (which can be done with a nice classical mechanical analog as described in the paper), we solve the dynamic problem similar to [Paris-Mandoki et al., *"Superfluid flow above the critical velocity"* Scientific Reports, 7 (2015) 9070](https://doi.org/10.1038/s41598-017-08941-8) where we slowly turn on the potential.

```{code-cell} ipython3
def go(v=1.5, U0=2, sigma=10.0, f=8, **kw):
    kw.update(V_v_c=v, V0_mu=U0, V_t_=50.0, phi0=0.0, V_sigma_healing_length=sigma)
    kw.update(Nxyz=(256 * f,), Lxyz=(10.0 * f,))
    e = Experiment(**kw)
    s = e.get_state(cool=False)
    s.cooling_phase = 1

    T = e.Lxyz[0] / e.c_sound
    ev = EvolverABM(s, dt=0.1 * s.t_scale)
    steps = 200
    # fig, axs = plt.subplots(1, 2, figsize=(10, 5))
    fig, ax = plt.subplots()
    axs = [ax]
    skip = 10
    frames = int(np.ceil(T / ev.dt / steps))
    for frame in FPS(frames=frames, timeout=60 * 5):
        ev.evolve(steps)
        if frame % skip == 0:
            ax = axs[0]
            ax.cla()
            plt.sca(ax)
            ev.y.plot(comoving=True)
            display(fig)
            clear_output(wait=True)
    return ev.get_y()
```

```{code-cell} ipython3
# Some points from Fig. 1.
# b)
go(U0=2.0, v=1.0)
```

```{code-cell} ipython3
# a) Here g*n_min = mu - U0, v_c/c_sound = sqrt(1-U0/mu)

#go(U0=0.5, v=0.4*np.sqrt(0.5))
s = go(U0=2, v=0.1)
```

```{code-cell} ipython3
# d)
s = go(U0=-0.4, v=2.0, V_integrable_mu=0.001, f=8)
```

```{code-cell} ipython3
f = 2
e = Experiment(V_v_c=0.9, v_x_V_v=1, V0_mu=0.1, V_t_=100.0, phi0=0.0, t_cool=0.1,
               
               Nxyz=(256*f,), Lxyz=(10.*f,))
s = e.get_state(cool=True)
s.cooling_phase = 1
ev = EvolverABM(s, dt=0.1*s.t_scale)
steps = 200
fig, axs = plt.subplots(1, 2, figsize=(10, 5))
Fs = [s.get_force()]
ps = [s.get_momentum()]
ts = [s.t]
skip = 10
for frame in FPS(frames=10000, timeout=60*5):
    ev.evolve(steps)
    ts.append(ev.y.t)
    Fs.append(ev.y.get_force())
    ps.append(ev.y.get_momentum())
    if frame % skip == 0:
        ax = axs[0]
        ax.cla()
        plt.sca(ax)
        ev.y.plot(comoving=True)

        ax = axs[1]
        ax.cla()
        plt.sca(ax)
        Fmax = np.abs(Fs).max()
        pmax = np.abs(ps).max()
        plt.plot(ts, np.divide(Fs, Fmax))
        plt.plot(ts, np.divide(ps, pmax))
        ax.set(title=f"{Fmax=:0.2g} ({np.mean(Fs):.4g}), {pmax=:0.2g} ({np.mean(ps):.4g})")
        #plt.axhline(np.mean(Fs))
        ax.set(ylim=(-1,1))
        display(fig)
        clear_output(wait=True)
```

```{code-cell} ipython3
from ipywidgets import interact
def g(n):
    return n

def G(n):
    return n**2/2

@interact(v=(0, 3.0), U0=(-1.0, 1.0))
def draw_W(v=0.1, U0=0.0):
    n = np.linspace(0.5, 2, 500)[1:]
    W = v**2/2*(n + 1/n) + n*g(1) - G(n)
    W_1 = v**2/2*(1 + 1/1) + 1*g(1) - G(1)
    W0 = W - U0*n
    W0_1 = W_1 - U0*1
    plt.plot(np.sqrt(n), W-W_1)   
    plt.plot(np.sqrt(n), W0-W0_1)
```

```{code-cell} ipython3

```
