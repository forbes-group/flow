"""1D trap oscillations past a weak barrier."""

from collections import namedtuple

from pathlib import Path
import os

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from tqdm import tqdm

import mmfutils.performance.fft, mmfutils.plot
from mmfutils.contexts import FPS
from pytimeode.evolvers import EvolverSplit, EvolverABM

from gpe.bec import StateBase, Units
from gpe.minimize import MinimizeState, MinimizeStateFixedPhase

from gpe import utils

StateBaseGPU = None
try:
    import gpe.gpu

    if gpe.gpu.cupy:
        from gpe.gpu.bec import StateBase as StateBaseGPU
except ImportError:
    pass

mmfutils.performance.fft.set_num_threads(1)

u = Units()


def _get_res(name="Results", **kw):
    """Return a namedtuple with the arguments."""
    names = list(kw.keys())
    Results = namedtuple(name, names)
    return Results(**kw)


class StateMixin:
    """State supporting flow in 1D.

    We create states with various amounts of "turbulence" oscillating in an HO
    potential, then see how the CM motion decays when we insert a finger.
    """

    def __init__(self, experiment, **kw):
        self.experiment = experiment
        self._computing_E = False
        super().__init__(**kw)

    ######################################################################
    # These will ultimately be rolled into gpe.bec.StateBase
    def get_xp(self, gpu=False):
        try:
            return super().get_xp(gpu=gpu)
        except AttributeError:
            return np

    ######################################################################
    def get_energy(self):
        """Wrapper to set `self._computing_E` so we can turn off stochastic
        potential."""
        self._computing_E = True
        try:
            return super().get_energy()
        finally:
            self._computing_E = False

    def _integrate_(self, a, **kw):
        """Return the integral of `a` over the box."""
        return self.xp.sum(self.metric * self.xp.asarray(a), **kw)

    def _get_Vext_(self):
        """Return the external potential."""
        return self.experiment.get_Vext(state=self, gpu=True)

    def get_x_cm(self):
        x = self.get_xyz()[0]
        x_cm = self.integrate(x * self.get_density()) / self.get_N()
        return x_cm

    def get_force(self):
        dV = self.experiment.get_Vfinger(state=self, d=1)
        n = self.get_density()
        return self.integrate(dV * n)

    def get_momentum(self):
        psi = self.get_psi()
        return self.hbar * self.braket(self.basis.get_gradient(psi)[0]).imag

    def plot(
        self, log=False, label=None, comoving=False, ax=None, smooth=1
    ):  # pragma: nocover
        n = self.get_density()

        def scale(n):
            if log:
                return np.log10(n)
            return n

        n = scale(n)
        x = self.xyz[0]
        xlabel = rf"$x$ [{self.experiment.x_label}]"

        x_ = x / self.experiment.x_unit
        if ax is None:
            ax = plt.gca()
        plt.sca(ax)
        ax.plot(x_, n, label=label)
        if smooth:
            k = self.asnumpy(self.basis.kx)
            sigma_k = 2 * np.pi / self.experiment.V_sigma / smooth
            n_smooth = np.fft.ifft(np.exp(-(k**2) / 2 / sigma_k**2) * np.fft.fft(n))
            ax.plot(x_, n_smooth, c="C2", alpha=0.5)
        ax.set(xlabel=xlabel)
        E = self.get_energy()
        N = self.get_N()
        t = np.ravel(self.t)[0]
        t_ = t / self.experiment.t_unit
        t_name = self.experiment.t_name
        ax.set(title=f"$t={t_:.4f}{t_name}$, ${N=:.4f}$, ${E=:.4f}$")


class StateCPU(StateMixin, StateBase):
    pass


State = StateCPU

if StateBaseGPU:

    class StateGPU(StateMixin, StateBaseGPU):
        pass


class StochasticMixin:
    stochastic_dt_t_scale = 10
    stochastic_V0_mu = 0.1
    stochastic_cool = 0.01
    stochastic_cool_only = False
    stochastic_kc_kmax = 0.5
    E = None

    def init(self):
        super().init()
        self.stochastic_V0 = self.stochastic_V0_mu * self.mu
        self._E = self.E
        if self.stochastic_kc_kmax:
            dx = self.Lx / self.Nx
            kmax = np.pi / dx
            kc = self.stochastic_kc_kmax * kmax
            abs_kx = np.abs(2 * np.pi * np.fft.fftfreq(self.Nx, dx))
            self._where_kc = abs_kx <= kc

    def get_stochastic_V(self, state, gpu=False):
        """ "Return the stochastic potential.

        This is simply a randomly varying potential with normally distributed values.
        Note that it is therefore NOT suitable for the EvolverABM, but should work with
        the EvolverSplit.
        """
        if state.initializing or state._computing_E or self.stochastic_V0 == 0:
            return 0

        if self._E is None:
            self._E = state.get_energy()
        else:
            dE = (state.get_energy() - self._E) / abs(self._E)
            # dE = max(min(dE, 1), -1)
            if self.stochastic_cool_only:
                # Only allow cooling.
                dE = max(0, dE)
            cooling_phase = 1 + 1j * self.stochastic_cool * dE
            state._phase = 1.0 / 1j / state.hbar / cooling_phase * abs(cooling_phase)

        # Need to check the scale...  Probably should be a dt scaling too.
        V = self.rng.normal(size=state.shape, scale=1 / np.sqrt(np.prod(state.shape)))
        if self.stochastic_kc_kmax:
            V = np.fft.ifft(np.where(self._where_kc, np.fft.fft(V), 0)).real

        return self.stochastic_V0 * V


def evolve_stochastic(state, T=1.0, Nt=100, dt_t_scale=1.0, dphi=0.1, rng=None, seed=2):
    """Evolve the state with stochastic fluctuations.

    We use complex chemical potentials to keep E fixed.
    """
    if rng is None:
        rng = np.random.default_rng(seed=seed)

    dT = T / Nt
    dt = dt_t_scale * state.t_scale
    steps = max(int(np.ceil(dT / dt), 2))
    E0 = state.get_energy()
    for n in range(Nt):
        phase = np.exp(2j * dphi * (rng.random(size=state.shape) - 0.5))


class ExperimentBase(utils.ExperimentBase):
    """Base experiment.

    The external potential here is always a harmonic trap, so we give special attention
    to evolution, allowing for comparisons at each oscillation.

    The parameters here are to ensure converged results, but are not really suitable for
    experimental work.  See ExperimentLab for parameters motivated by experiments.

    Parameters
    ----------
    Nx : int
        Number of points.  Sets the computational difficulty.
    Lx : float
        Size of box.
    healing_length_dx : float
        UV convergence.  Should be 2 or higher.
    x_TF_R : float
        IR convergence.  Fraction of `R=Lx/2` occupied by the trap.  Must be large
        enough to accommodate excitations.
    x0_R : float
        Shift to induce oscillations as a fraction of `R`.
    """

    data_dir = "_data"

    Nx = 128
    Lx = 10.0
    healing_length_dx = 2.0
    hbar = m = 1
    x0_R = 0.1
    x_TF_R = 0.5

    n0 = 1.0
    Ntot = None  # Allows one to fix the particle number.  See get_initial_state()

    V0_mu = 0.1
    V_sigma_healing_length = 3.0
    V_t_ = 0.0  # How long to take to turn barrier on in units of t_unit
    V_v_c = 0.0  # Speed of potential in units of c_cound
    V_integrable_mu = 0.0  # How much to break integrability

    State = StateGPU if StateBaseGPU else StateCPU
    StateCPU = StateCPU
    phi0 = 0.01  # Size of random phases

    t_cool = 1.0
    seed = 2

    soliton_spacing_healing_length = 10.0
    initial_state = "regular_solitons"  # Or 'random' or 'random_solitons'

    # For evolve()
    Nt = 200
    dt_t_scale = 0.2
    image_ts_ = [100]

    def init(self):
        dx = self.Lx / self.Nx
        self.healing_length = self.healing_length_dx * dx
        self.mu = self.hbar**2 / 2 / self.m / self.healing_length**2
        R = self.Lx / 2
        self.x0 = self.x0_R * R
        self.x_TF = self.x_TF_R * R
        self.wx = np.sqrt(2 * self.mu / self.m) / self.x_TF
        self.Tx = 2 * np.pi / self.wx

        self.t_unit = 2 * np.pi / self.wx
        self.t_name = r"T_x"

        self.g = self.mu / self.n0
        self.V_sigma = self.V_sigma_healing_length * self.healing_length

        self.V0 = self.V0_mu * self.mu
        # self.V0_t_ = utils.get_smooth_transition(
        #    [0, self.V0], durations=[0], transitions=[self.V_t_]
        # )

        self.c_sound = np.sqrt(self.mu / self.m)
        self.state_args = dict(
            experiment=self,
            Nxyz=(self.Nx,),
            Lxyz=(self.Lx,),
            g=self.g,
            hbar=self.hbar,
            m=self.m,
        )
        self.rng = np.random.default_rng(seed=self.seed)
        self.x_unit = self.healing_length
        self.x_label = r"$\xi$"

    def get_Vfinger(self, state, gpu=False, d=0):
        """Return the d'th derivative of the finger potential."""
        x = state.get_xyz(gpu=gpu)[0]
        xp = state.get_xp(gpu=gpu)
        V = self.V0 * xp.exp(-((x / self.V_sigma) ** 2) / 2)
        if d == 0:
            return V
        elif d == 1:
            dV = (-x / self.V_sigma**2) * V
            return dV
        else:
            raise NotImplementedError(f"{d=} not supported.")

    def get_Vext(self, state, gpu=False):
        """Return the external potential."""
        x = state.get_xyz(gpu=gpu)[0]
        x0 = 0.0
        if state.initializing or state.t < 0:
            x0 = self.x0
        V_ext = self.m * (self.wx * (x - x0)) ** 2 / 2
        V_ext += self.get_Vfinger(state=state, gpu=gpu)

        if (state.initializing or state.t < 0) and (mu := getattr(self, "mu", None)):
            V_ext -= mu
        return V_ext

    def get_state(self, initialize=False, gpu=True):
        State = self.State
        if not gpu:
            State = self.StateCPU
        state = State(**self.state_args)
        V = self.get_Vext(state)
        n = np.maximum((self.mu - V) / self.g, 0)
        state.set_psi(np.sqrt(n))
        return state

    def get_initial_state(self, cool=False):
        state = self.get_state(gpu=False)

        if self.initial_state == "random":
            state = MinimizeState(state, fix_N=False).minimize(use_scipy=True)
            psi = state.get_psi()
            phi = 2 * self.phi0 * (self.rng.random(size=psi.shape) - 0.5)
            state.set_psi(psi * np.exp(1j * phi))
        elif self.initial_state == "regular_solitons":
            x = state.get_xyz()[0]
            k = 2 * np.pi / (self.soliton_spacing_healing_length * self.healing_length)
            phase = np.round(np.sin(k * x))
            state = MinimizeStateFixedPhase(state, phase=phase, fix_N=False).minimize()
        else:
            raise AttributeError(f"Unknown {self.initial_state=}")

        if cool:
            state.t = -self.t_cool  # Cool over negative times before quench.
            state.cooling_phase = 1j
            dt = 0.1 * state.t_scale
            steps = max(int(np.ceil(self.t_cool / dt)), 2)
            ev = EvolverSplit(state, dt=self.t_cool / steps, normalize=True)
            ev.evolve(steps)
            psi = ev.y.get_psi()

            state = self.State(**self.state_args)
            state.set_psi(psi)

        _state = state
        state = self.get_state()
        state.set_psi(_state.get_psi())
        return state

    def evolve(
        self,
        state=None,
        states=None,
        periods=None,
        skip=100,
        timeout=None,
        show=True,
        log=False,
    ):
        """Evolve and save state.

        Arguments
        ---------
        periods : int
            Number of periods to evolve for.
        timeout : float
            Timeout in seconds.

        """
        if periods is None:
            periods = max(self.image_ts_)

        if states is None:
            if state is None:
                state = self.get_initial_state()
            states = [state]
        self._states = states

        fig_axs = None
        ts = [state.t for state in states]
        x_cms = [state.get_x_cm() for state in states]
        state = states[-1]

        dt = self.dt_t_scale * state.t_scale
        dT = self.Tx / self.Nt
        assert np.allclose(np.diff(ts), dT)

        steps = int(max(np.ceil(dT / dt), 2))
        dt = dT / steps
        ev = EvolverABM(state, dt=dt)

        frames = periods * self.Nt - len(states)
        fps = FPS(frames=frames, timeout=timeout)
        for frame in tqdm(fps, total=frames):
            ev.evolve(steps)
            states.append(ev.get_y())
            ts.append(ev.y.t)
            x_cms.append(ev.y.get_x_cm())
            if show and frame % skip == 0:
                fig_axs = self.show_frame(
                    states=states, log=log, fps=fps, fig_axs=fig_axs
                )
        fig_axs = self.show_frame(states=states, log=log, fps=fps, fig_axs=fig_axs)
        plt.close("all")
        return _get_res(
            "EvolveResults",
            Nt=self.Nt,
            ts=np.asarray(ts),
            x_cms=np.asarray(x_cms),
            states=states,
        )

    def run(self, periods=100, **kw):
        data_dir = Path(self.data_dir) / self.dir_name
        checkpoint_file = data_dir / "states.npy"

        dT = self.Tx / self.Nt

        if checkpoint_file.exists():
            psis = np.load(checkpoint_file)
            ts = np.arange(len(psis)) * dT
            states = []
            for psi, t in zip(psis, ts):
                state = self.get_state()
                state.set_psi(psi)
                state.t = t
                states.append(state)
        else:
            states = [self.get_initial_state()]
        res = self.evolve(states=states, **kw)

        if not data_dir.exists():
            os.makedirs(data_dir)
        psis = [s.get_psi() for s in res.states]
        np.save(checkpoint_file, psis)
        return res

    def show_frame(self, states, log=False, fps=None, fig_axs=None):
        from IPython.display import display, clear_output

        if fig_axs is None:
            fig, axs = plt.subplots(1, 2, figsize=(10, 5))
            axr = axs[0].twinx()
        else:
            fig, axs, axr = fig_axs

        fig_axs = fig, axs, axr
        ts = [state.t for state in states]
        x_cms = [state.get_x_cm() for state in states]

        state = states[-1]

        ax = axs[0]
        ax.cla()
        plt.sca(ax)
        state.plot(log=log)
        axr.plot(state.x / self.healing_length, state.get_Vext(), "C1")
        axr.set(ylim=(0, 1.5 * self.mu))
        ax = axs[1]
        ax.cla()
        plt.sca(ax)
        t_ = np.divide(ts, self.Tx)
        ax.plot(t_, x_cms)
        ax.plot(t_, self.x0 * np.cos(2 * np.pi * t_), "--")
        ax.set(xlabel="$t$ [$T_x$]", title=f"fps={str(fps)}" if fps else "")

        clear_output(wait=True)
        display(fig)
        return fig_axs


class Experiment(ExperimentBase):
    """

    The protocol is this:
    1. Find the lowest energy state displaced by x0.
    2. Imprint random phases (cool slightly).
    3. Quench to x0 = 0
    4. Evolve.
    """

    Nx = 256
    Lx = 10.0


class ExperimentSolitons(ExperimentBase):
    """Regular solitons."""

    Ns = 1  # Number of solitons
    ds_healing_length = 3.5  # Spacing between solitons
    initial_state = "regular_solitons"  # or 'random_solitons'

    def get_initial_state(self, cool=False):
        state = self.get_state(gpu=False)

        fix_N = False
        if self.Ntot is not None:
            state.set_psi(np.sqrt(self.Ntot / state.get_N()) * state.get_psi())
        state = MinimizeState(state, fix_N=fix_N).minimize(use_scipy=True)
        psi = state.get_psi()

        (x,) = state.xyz
        if self.initial_state == "regular_solitons":
            # Produces centered solitons.
            ds = self.ds_healing_length * self.healing_length
            xs = ds * (np.arange(self.Ns) - self.Ns / 2 + 0.5)
        elif self.initial_state == "random_solitons":
            xs = self.x_TF * (2 * self.rng.random(size=self.Ns) - 1)
        else:
            raise AttributeError(f"Unknown {self.initial_state=}")

        phase = np.prod([np.sign(x - self.x0 - x_) for x_ in xs], axis=0)
        phase += np.zeros_like(x)

        state = MinimizeStateFixedPhase(state, phase=phase, fix_N=fix_N).minimize()

        if cool:
            state.t = -self.t_cool  # Cool over negative times before quench.
            state.cooling_phase = 1j
            dt = 0.1 * state.t_scale
            steps = max(int(np.ceil(self.t_cool / dt)), 2)
            ev = EvolverSplit(state, dt=self.t_cool / steps, normalize=True)
            ev.evolve(steps)
            psi = ev.y.get_psi()
            state.set_psi(psi)

        _state = self.get_state()
        _state.set_psi(state.get_psi())

        return _state
