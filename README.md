Superfluid Flow
===============
[![gitlab pipeline status](https://gitlab.com/forbes-group/flow/badges/main/pipeline.svg)](https://gitlab.com/forbes-group/flow/-/commits/main)
[![gitlab coverage report](https://gitlab.com/forbes-group/flow/badges/main/coverage.svg)](https://gitlab.com/forbes-group/flow/-/commits/main)


## Viscosity Characterization

This is the summer project with Chance Baggett.

## CoCalc

To build the documentation on CoCalc, open the following file:

* [`SphinxAutoBuildServer.term`](SphinxAutoBuildServer.term)

This will build the documentation, and serve it on `https://cocalc.com/<project
id>/raw/<project folder>/Docs/_build/html/index.html`.  You must currently fill in the
folder and CoCalc project id.

## TL;DR

Get started on CoCalc by:

```bash
pip install mmf-setup
mmf_setup cocalc

# If using CoCalc Docker:
for app in mercurial black jupytext pdm poetry; do
    pipx install $app
done
pipx inject mercurial hg-git hg-evolve
curl micro.mamba.pm/install.sh | bash

mkdir repositories
hg clone git@gitlab.com:forbes-group/flow.git repositories/flow
ln -s repositories/flow .

cd flow

####### THIS NEEDS WORK!
# Clone various repos into _ext... this needs work
mkdir -p _ext
for repo in pytimeode gpe; do
  hg init ~/repositories/$repo && \
    echo '[phases]' >> ~/repositories/$repo/.hg/hgrc && \
    echo 'publish = False' >> ~/repositories/$repo/.hg/hgrc
  ln -s ~/repositories/$repo _ext/$repo
done

for repo in pytimeode gpe; do
  # Update repo: See below.
  pushd ~/repositories/$repo && hg update tip && popd
  # Clone repo.  See below.
done

# This does not currently work
hg clone ssh://heptapod/forbes-group/pytimeode ~/repositories/pytimeode
hg clone ssh://heptapod/forbes-group/gpe ~/repositories/gpe

# Instead, I need to push from my machine:

hg push -r . ssh://ccp_flow/repositories/pytimeode
hg push -r . ssh://ccp_flow/repositories/gpe

# etc.

make init
```


## Overview

We control the building of environments etc. with `Makefile`.  This contains the
following targets:

* `make init`: This should provision the environment, install all of the required
  software, and then create a kernel named `flow_gpu` that can be used in Notebooks and
  on various CoCalcs.
* `make shell`: Runs a new bash shell with the environment active.  This can be used,
  for example, to run tests.  *This shell can be closed with `ctrl-d`: i.e. it runs as a
  separate process like `poetry shell`, not like an activated environment.*
* `make sync`: Run `jupytext` to synchronize all notebooks with their corresponding
  Markdown files.  Note: In general, the `*.ipynb` notebooks should **not** be committed
  to the version control because they tend to have large images etc.  Instead, they
  should be paired with a markdown file:
  
  ```bash
  make shell
  jupytext --set-format ipynb,myst <file>.ipynb
  hg add <file>.md
  make sync
  ```
  
  Then, commit the generated `*.md` file and commit updates after running `make sync`.
  Be sure to run `make sync` after you pull changes too.  This could be done with pre-
  and post- commit hooks.
  
* `make realclean`: Remove all the environments.

Dependencies are specified in:

* `environment.yaml`: This should include especially things that are not pure python,
  like `cupy` which requires also installing appropriate versions of the NVIDIA Cuda
  libraries.
* `pyproject.toml`: The remaining pure python dependencies should be managed here, with
  tools like [PDM][] or [Poetry][].

## Version Control

Make sure you have `mercurial` installed with the `hg-git` and `evolve` extensions
installed and enabled.  This can be done with `pipx` for example

```bash
for app in mercurial black jupytext pdm poetry; do
    pipx install $app
done
pipx inject mercurial hg-git hg-evolve
```

Your `~/.hgrc` file should include at least the following:

```ini
# ~/.hgrc or a file that it includes
[ui]
#username = Michael McNeil Forbes <michael.forbes+python@gmail.com>
username = $LC_HG_USERNAME   # We use this on CoCalc... you must SendEnv with ssh.

[extensions]
evolve =
hggit =
topic =
graphlog =
histedit =
rebase =
record =
shelve =
```

```bash
pip install mmf-setup
mmf_setup cocalc

# If using CoCalc Docker - i.e. on Penguin, you should also install the following.
# These are not needed on the official CoCalc server which includes them by default.
# We might switch to including these in the Docker image later.
for app in mercurial black jupytext pdm poetry; do
    pipx install $app
done
pipx inject mercurial hg-git hg-evolve
```

## Installation

(Notes from Ed... might be out of date)
1. Create a directory `_ext` and symlink the `mmfutils`, `gpe`, and `pytimeode`
   repositories there.
2. If using git, install pre-commit hooks: `pdm run pre-commit install`
6. Sync the myst notebooks:  `pdm run jupytext --sync md_notebooks/*/*`

See `Docs/index.md` for more details.
####### Ancestor
See `Docs/index.md` for more details.


======= end

# Funding

<a href="https://www.nsf.gov"><img width="10%"
src="https://nsf.widen.net/content/txvhzmsofh/png/" />
</a>
<br>

Some of the material presented here is based upon work supported by the National Science
Foundation under [Grant Number 2012190](https://www.nsf.gov/awardsearch/showAward?AWD_ID=2012190). Any opinions, findings, and conclusions or
recommendations expressed in this material are those of the author(s) and do not
necessarily reflect the views of the National Science Foundation.


[PDM]: <https://pdm.fming.dev/latest/>
[Poetry]: <https://poetry.eustace.io> "Python packaging and dependency management made easy."

