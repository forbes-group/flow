"""1D Flow past a weak barrier."""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

import mmfutils.performance.fft, mmfutils.plot
from mmfutils.contexts import FPS
from pytimeode.evolvers import EvolverSplit, EvolverABM

from gpe.bec import StateTwist_x, Units
from gpe.minimize import MinimizeState

from gpe import utils

mmfutils.performance.fft.set_num_threads(1)


class State(StateTwist_x):
    """State supporting flow in 1D.

    We create states with various amounts of "turbulence", and then insert a gaussian
    "finger" and measure the force acting on this finger, and how the net momentum of
    the state is changed.
    """

    def __init__(self, experiment, **kw):
        self.experiment = experiment
        super().__init__(**kw)

    def _get_Vext_(self):
        """Return the external potential."""
        return self.experiment.get_Vext(state=self, gpu=True)

    def get_force(self):
        dV = self.experiment.get_Vfinger(state=self, d=1)
        n = self.get_density()
        return self.integrate(dV * n)

    def get_momentum(self):
        psi = self.get_psi()
        return self.hbar * self.braket(self.basis.get_gradient(psi)[0]).imag

    def plot(self, log=False, label=None, comoving=False):  # pragma: nocover
        n = self.get_density()

        def scale(n):
            if log:
                return np.log10(n)
            return n

        n = scale(n)
        if comoving:
            x = self.xyz[0]
            xlabel = r"$x$ [$\xi$]"
        else:
            x = self.x_lab
            xlabel = r"$x_{\rm lab}$ [$\xi$]"
            inds = np.argsort(x)
            x, n = x[inds], n[inds]

        x_ = x / self.experiment.healing_length
        plt.plot(x_, n, label=label)
        ax = plt.gca()
        ax.set(xlabel=xlabel)
        E = self.get_energy()
        N = self.get_N()
        t = np.ravel(self.t)[0]
        t_ = t / self.experiment.t_unit
        t_name = self.experiment.t_name
        plt.suptitle(f"$t={t_:.4f}{t_name}$, ${N=:.4f}$, ${E=:.4f}$")


class Experiment(utils.ExperimentBase):
    """

    The protocol is this:
    1. Find the lowest energy state with a sustained flow past the finger.
    2. Imprint random phases.
    3. Evolve.
    """

    Nxyz = (256,)
    Lxyz = (10.0,)
    healing_length_dx = 2.0
    hbar = m = 1
    n0 = 1

    V0_mu = 0.1
    V_sigma_healing_length = 3.0
    V_t_ = 0.0  # How long to take to turn barrier on in units of t_unit
    V_v_c = 0.0  # Speed of potential in units of c_cound
    V_integrable_mu = 0.0  # How much to break integrability

    State = State
    phi0 = 0.01  # Size of random phases

    v_x_V_v = 1.0  # Speed of frame in units of V_v
    t_cool = 1.0

    def init(self):
        dx = self.Lxyz[0] / self.Nxyz[0]
        self.healing_length = self.healing_length_dx * dx
        self.mu = self.hbar**2 / 2 / self.m / self.healing_length**2

        self.t_unit = self.hbar / self.mu
        self.t_name = r"\hbar/\mu"

        self.g = self.mu / self.n0
        self.V_sigma = self.V_sigma_healing_length * self.healing_length

        self.V0 = self.V0_mu * self.mu
        self.V0_t_ = utils.get_smooth_transition(
            [0, self.V0], durations=[0], transitions=[self.V_t_]
        )

        self.c_sound = np.sqrt(self.mu / self.m)
        self.V_v = self.V_v_c * self.c_sound
        self.v_x = self.v_x_V_v * self.V_v
        self.state_args = dict(
            experiment=self,
            Nxyz=self.Nxyz,
            Lxyz=self.Lxyz,
            g=self.g,
            hbar=self.hbar,
            m=self.m,
            v_x=self.v_x,
        )
        self.rng = np.random.default_rng(seed=2)

    def get_Vfinger(self, state, gpu=False, d=0):
        """Return the d'th derivative of the finger potential."""
        t = state.t
        t_ = t / self.t_unit

        x_lab = state.get_xyz(gpu=gpu)[0] + state.v_x * t
        x0 = self.V_v * t

        Lx = self.Lxyz[0]
        x0 = (x0 - x_lab.min()) % Lx + x_lab.min()

        # We add an image on either side of the box to ensure strict periodicity.
        V = self.V0_t_(t_) * sum(
            np.exp(-(((x_lab - x0) / self.V_sigma) ** 2) / 2)
            for x0 in [x0 - Lx, x0, x0 + Lx]
        )
        if d == 0:
            return V
        elif d == 1:
            dV = self.V0 * sum(
                -(x_lab - x0)
                / self.V_sigma**2
                * np.exp(-(((x_lab - x0) / self.V_sigma) ** 2) / 2)
                for x0 in [x0 - Lx, x0, x0 + Lx]
            )
            return dV
        else:
            raise NotImplementedError(f"{d=} not supported.")

    def get_Vext(self, state, gpu=False):
        """Return the external potential."""
        t = state.t

        x_lab = state.get_xyz(gpu=gpu)[0] + state.v_x * t

        Lx = self.Lxyz[0]
        ks = np.arange(1, 5) * np.pi / Lx

        # Background to break integrability
        V_ext = (
            self.V_integrable_mu
            * self.mu
            * sum(np.sin(k * x_lab) for a, k in zip(ks, [1.0, -2.0, 3.0, 1.0]))
        )
        V_ext += self.get_Vfinger(state=state, gpu=gpu)

        if (state.initializing or state.t < 0) and (mu := getattr(self, "mu", None)):
            V_ext -= mu
        return V_ext

    def get_state(self, cool=False):
        state = self.State(**self.state_args)
        V = self.get_Vext(state)
        n = np.maximum((self.mu - V) / self.g, 0)
        state.set_psi(np.sqrt(n))
        return state

        phi = 2 * self.phi0 * (self.rng.random(size=n.shape) - 0.5)
        state.set_psi(np.sqrt(n) * np.exp(1j * phi))
        if cool:
            state.cooling_phase = 1j
            dt = 0.1 * state.t_scale
            steps = max(int(np.ceil(self.t_cool / dt)), 2)
            ev = EvolverSplit(state, dt=self.t_cool / steps, normalize=True)
            ev.evolve(steps)
            psi = ev.y.get_psi()

            state = self.State(**self.state_args)
            state.set_psi(psi)
        return state

    def get_initial_state(self, cool=False):
        state = self.get_state()
        state = MinimizeState(state, fix_N=False).minimize(use_scipy=True)
        psi = state.get_psi()
        phi = 2 * self.phi0 * (self.rng.random(size=psi.shape) - 0.5)
        state.set_psi(psi * np.exp(1j * phi))
        if cool:
            state.cooling_phase = 1j
            dt = 0.1 * state.t_scale
            steps = max(int(np.ceil(self.t_cool / dt)), 2)
            ev = EvolverSplit(state, dt=self.t_cool / steps, normalize=True)
            ev.evolve(steps)
            psi = ev.y.get_psi()

            state = self.State(**self.state_args)
            state.set_psi(psi)
        return state

    def evolve(self, state=None, steps=200, frames=10000, timeout=60 * 5, show=True):
        if state is None:
            state = self.get_initial_state()
        ev = EvolverABM(state, dt=0.1 * state.t_scale)
        fig, axs = plt.subplots(1, 2, figsize=(10, 5))
        Fs = [state.get_force()]
        ps = [state.get_momentum()]
        ts = [state.t]
        skip = 10
        for frame in FPS(frames=frames, timeout=timeout):
            ev.evolve(steps)
            ts.append(ev.y.t)
            Fs.append(ev.y.get_force())
            ps.append(ev.y.get_momentum())
            if show and frame % skip == 0:
                from IPython.display import display, clear_output

                ax = axs[0]
                ax.cla()
                plt.sca(ax)
                ev.y.plot()

                ax = axs[1]
                ax.cla()
                plt.sca(ax)
                Fmax = np.abs(Fs).max()
                pmax = np.abs(ps).max()
                plt.plot(ts, np.divide(Fs, Fmax))
                plt.plot(ts, np.divide(ps, pmax))
                ax.set(
                    title=f"{Fmax=:0.2g} ({np.mean(Fs):.4g}), {pmax=:0.2g} ({np.mean(ps):.4g})"
                )
                # plt.axhline(np.mean(Fs))
                ax.set(ylim=(-1, 1))

                display(fig)
                clear_output(wait=True)
        return np.asarray(ts), np.asarray(ps), np.asarray(Fs)
